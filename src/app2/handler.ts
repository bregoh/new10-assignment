import { StatusEnum } from '../shared/dto/Loan.Dto';
import { LoanService } from '../shared/service/Loan.service';
import Response from '../shared/utils/Response';

const service = new LoanService();

export const disburseLoanHandler = async (event: any) => {
    try {
        const id = event.pathParameters.id;
        const body = { status: StatusEnum.DISBURSED };
        const result = await service.updateLoan(id, body);
        return Response.success(result, 200, 'success');
    } catch (error: any) {
        return Response.error(
            error.details[0].context,
            400,
            error.details[0].message,
        );
    }
};
