// import { Model } from 'dynamodb';
import { LoanService } from '../shared/service/Loan.service';
import LoanEntity from '../shared/entity/Loan.entity';
import LoanDto, { StatusEnum } from '../shared/dto/Loan.Dto';

describe('Test the endpoint', () => {
    LoanEntity.tableName = 'TestLoan';
    const service = new LoanService();
    const LoanTable = LoanEntity.init();
    const body: LoanDto = { amount: 800 };
    let id: string;

    it('should create a new loan', async () => {
        const result = await service.createLoan(body);
        id = result.attrs.id;
        expect(result.attrs.amount).toEqual(body.amount);
        expect(result.attrs).toHaveProperty('status');
        expect(result.attrs).toHaveProperty('id');
        expect(result.attrs).toHaveProperty('createdAt');
    });

    it('should get all loan', async () => {
        const result = await service.getAllLoan();
        expect(result.length).toBeGreaterThanOrEqual(1);
        expect(result[0].attrs).toHaveProperty('amount');
        expect(result[0].attrs).toHaveProperty('status');
        expect(result[0].attrs.amount).toEqual(body.amount);
    });

    it('should get one loan', async () => {
        const result = await service.getOneLoan(id);
        expect(result.attrs.amount).toEqual(body.amount);
        expect(result.attrs).toHaveProperty('amount');
        expect(result.attrs).toHaveProperty('status');
    });

    it('should update a loan', async () => {
        const data = { status: StatusEnum.DISBURSED };
        const result = await service.updateLoan(id, data);
        expect(result.attrs).toHaveProperty('status');
        expect(result.attrs.status).toEqual(StatusEnum.DISBURSED);
    });

    it('should delete a loan', async () => {
        const result = await service.deleteOneLoan(id);
        expect(result).toBeTruthy();
    });

    afterAll(() => {
        LoanTable.deleteTable((err: any) => {
            if (err) {
                throw new Error(err);
            }
        });
    });
});
