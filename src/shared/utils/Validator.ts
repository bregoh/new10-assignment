import Joi from 'joi';

const Validator = Joi.object({
    amount: Joi.number(),
});

export default Validator;
