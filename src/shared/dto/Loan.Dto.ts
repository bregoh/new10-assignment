export enum StatusEnum {
    CREATED = 'created',
    DISBURSED = 'disbursed',
}

class LoanDto {
    amount: number;
    status?: StatusEnum;
    id?: string;
    readonly createdAt?: string;
}

export default LoanDto;
