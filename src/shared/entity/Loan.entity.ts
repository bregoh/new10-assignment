import * as DynamoDB from 'dynamodb';
import Joi from 'joi';

class LoanEntity {
    static tableName: string;

    static init() {
        return DynamoDB.define(LoanEntity.tableName, {
            hashKey: 'id',
            timestamps: true,
            schema: {
                id: DynamoDB.types.uuid(),
                amount: Joi.number().required(),
                status: Joi.string().required(),
            },
        });
    }
}

export default LoanEntity;
