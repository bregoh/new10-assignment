import * as DynamoDB from 'dynamodb';

class Model {
    constructor() {
        DynamoDB.AWS.config.update({
            region: 'localhost',
            endpoint: 'http://localhost:8000',
        });
    }

    async createTables(): Promise<any> {
        await new Promise((resolve, reject) => {
            DynamoDB.createTables((err) => (err ? reject(err) : resolve(null)));
        });
    }
}

export default Model;
