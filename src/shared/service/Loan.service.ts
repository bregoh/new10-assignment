import LoanDto, { StatusEnum } from '../dto/Loan.Dto';
import Model from '../model';
import LoanEntity from '../entity/Loan.entity';

export class LoanService extends Model {
    private Loan = LoanEntity.init();

    constructor() {
        super();
    }

    async createLoan(data: LoanDto): Promise<any> {
        await this.createTables();

        data.status = StatusEnum.CREATED;
        const result = new this.Loan(data);
        await result.save();

        return result;
    }

    async getAllLoan(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.Loan.scan()
                .loadAll()
                .exec((error: any, data: any) => {
                    if (error) {
                        reject(error);
                    }

                    resolve(data.Items);
                });
        });
    }

    async getOneLoan(id: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.Loan.query(id).exec((error: any, data: any) => {
                if (error) {
                    reject(error);
                }

                resolve(data.Items[0]);
            });
        });
    }

    async updateLoan(id: string, data: any): Promise<any> {
        return new Promise((resolve, reject) => {
            this.Loan.update({ id: id, ...data }, (error: any, loan: any) => {
                if (error) {
                    reject(error);
                }

                resolve(loan);
            });
        });
    }

    async deleteOneLoan(id: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.Loan.destroy(id, (error: any) => {
                if (error) {
                    reject(error);
                }

                resolve(true);
            });
        });
    }
}
