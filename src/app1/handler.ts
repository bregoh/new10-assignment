import LoanEntity from '../shared/entity/Loan.entity';
import LoanDto from '../shared/dto/Loan.Dto';
import { LoanService } from '../shared/service/Loan.service';
import Response from '../shared/utils/Response';
import Transformer from '../shared/utils/Transformer';

LoanEntity.tableName = 'Loan';
const service = new LoanService();

export const createLoanHandler = async (event: any) => {
    try {
        const body: LoanDto = await Transformer(event.body);
        const result = await service.createLoan(body);
        return Response.success(result, 201, 'success');
    } catch (error: any) {
        return Response.error(
            error.details[0].context,
            400,
            error.details[0].message,
        );
    }
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export const fetchAllLoanHandler = async (_event: any) => {
    try {
        const result = await service.getAllLoan();
        return Response.success(result, 200, 'success');
    } catch (error: any) {
        return Response.error(
            error.details[0].context,
            400,
            error.details[0].message,
        );
    }
};

export const fetchOneLoanHandler = async (event: any) => {
    try {
        const id = event.pathParameters.id;
        const result = await service.getOneLoan(id);
        return Response.success(result, 200, 'success');
    } catch (error: any) {
        return Response.error(
            error.details[0].context,
            400,
            error.details[0].message,
        );
    }
};

export const updateLoanHandler = async (event: any) => {
    try {
        const id = event.pathParameters.id;
        const base_url = process.env.BASE_URL;
        return Response.redirect(`${base_url}/dev/disburse/${id}`);
    } catch (error: any) {
        console.log('error', error);
        return Response.error(
            error.details[0].context,
            400,
            error.details[0].message,
        );
    }
};

export const deleteLoanHandler = async (event: any) => {
    try {
        const id = event.pathParameters.id;
        await service.deleteOneLoan(id);
        return Response.success({}, 204, 'success');
    } catch (error: any) {
        return Response.error(
            error.details[0].context,
            400,
            error.details[0].message,
        );
    }
};
